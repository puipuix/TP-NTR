# App Python

## Variables

**account**: Use to select the bank account id. An non-existent value will raise error 404. 

## Functions

**balance()**: Display the balance of the account.

**operations()**: Display all operations of the account.

**operations(*title*)**: Display all operations of the account that contains the parameter in their title.