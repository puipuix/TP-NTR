import requests
from datetime import datetime

account = 1

def balance():
    r = requests.get('http://localhost:8090/accounts/%d' % account)
    if 200 <= r.status_code and r.status_code < 300:
        print('My balance is %.2f' % r.json()['balance'])
    else:
        r.raise_for_status()

def operations(title=None):
    r=None
    if title:
        r = requests.get('http://localhost:8090/operations/account/%d/title/%s' % (account, title))
    else:
        r = requests.get('http://localhost:8090/operations/account/%d' % account)
        
    if 200 <= r.status_code and r.status_code < 300:
        if title:
            print('My operations that contains "%s" in their title are' % title)
        else:
            print('My operations are')
        json = r.json()
        for op in json:
            print(op['title'],': ',op['amount'],datetime.fromtimestamp(op['dat']/1000).strftime('(%Y/%m/%d, %H:%M:%S)'))
    else:
        r.raise_for_status()