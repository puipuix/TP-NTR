# App CSharp

Print the balance of the given account id.

## Commande Line Argument
**Example**: *./CSharp 1*

**arg1**: the account id.