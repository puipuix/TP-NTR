﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Collections.Generic;
using System.Data;

namespace CSharp
{
    class Program
    {
        /// <summary>
        /// Clear and print to the console.
        /// </summary>
        /// <param name="txt"></param>
        static void Print(string txt)
        {
            Console.Clear();
            Console.WriteLine(txt);
        }

        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                if (int.TryParse(args[0], out int client))
                {
                    try
                    {
                        // Connection
                        Print("Connecting to http://localhost:8090/accounts/" + client + "...");
                        var task = new HttpClient().GetAsync("http://localhost:8090/accounts/" + client);
                        task.Wait();

                        // Reading content
                        Print("Connected. Getting content...");
                        var response = task.Result.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
                        response.Wait();

                        // Deserialize JSON
                        var body = JsonSerializer.Deserialize<Dictionary<string, object>>(response.Result);
                        Print("The balance of the account " + client + " is " + body["balance"] + ".");
                    }
                    catch (HttpRequestException e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("HTTP Error : " + e.Message);
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Parse error: '" + args[0] + "' is not int value");
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Expected 1 argument! Got: " + args.Length);
            }
        }
    }
}
