package transactions;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Bank operations.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Operation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -116727086738073355L;
	
	/**
	 * The ID of the operation.
	 */
	private int id;
	
	/**
	 * Title of the operation.
	 */
	private String title;
	
	/**
	 * Amount of money added (positive) or deduced (negative).
	 */
	private double amount;

	/**
	 * The date of the operation.
	 */
	private long dat;
	
	/**
	 * The account id of that operation.
	 */
	private int account;
	
}
