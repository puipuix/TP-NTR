package transactions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/**
 * Contain request for payback and debit.
 * 
 * Path: http://localhost:8080/WebServices/services/Transaction
 * 
 * @author Alexis Vernes
 * 
 */
public class Transaction {

	/**
	 * 
	 */
	public Transaction() {

	}

	/**
	 * Generic request function.
	 * 
	 * @param client The client bank id.
	 * @param title of the transaction.
	 * @param value of the transaction. (positive or negative)
	 * @return If the operation was successful and the server response.
	 * @throws IOException if an error occurs while asking the rest server.
	 */
	private Result postRequest(int client, String title, double value) throws IOException {
		Result result;

		URL url = new URL("http://localhost:8090/operations");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");

		connection.setRequestProperty("Content-Type", "application/xml");
		connection.setRequestProperty("Accept", "*/*");
		connection.setRequestProperty("User-Agent", "ServicesSOAP");

		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);

		connection.setDoOutput(true);
		StringBuilder xml = new StringBuilder();
		xml.append("<item><title>")
				.append(title)
				.append("</title><amount>")
				.append(value)
				.append("</amount><account>")
				.append(client)
				.append("</account></item>");
		connection.getOutputStream().write(xml.toString().getBytes());
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			// System.out.println(response.toString());
			result = Result.builder().ok(true).message("ok").build();
			try {
				XmlMapper mapper = new XmlMapper();
				result.setOperation(mapper.readValue(response.toString(), Operation.class));
				result.setError(null);
			} catch (Exception e) {
				e.printStackTrace();
				result.setError(new Error(System.currentTimeMillis(), 500, "Internal Server Error", e.toString(), ""));
			}
		} catch (Exception e) {
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			// System.out.println(response.toString());
			result = Result.builder()
					.ok(false)
					.message(e.toString())
					.operation(null)
					.build();
			try {
				XmlMapper mapper = new XmlMapper();
				result.setError(mapper.readValue(response.toString(), Error.class));
			} catch (Exception f) {
				e.printStackTrace();
				result.setError(new Error(System.currentTimeMillis(), 500, "Internal Server Error", f.toString(), ""));
			}
			response.toString();
		}
		connection.disconnect();
		return result;
	}

	/**
	 * Debit request function.
	 * 
	 * @param client The client bank id.
	 * @param title of the transaction.
	 * @param value of the transaction. (positive)
	 * @return If the operation was successful and the server response.
	 * @throws IOException if an error occurs while asking the rest server.
	 */
	public Result debit(int client, String title, double value) throws IOException {
		return postRequest(client, title, -value);
	}

	/**
	 * Payback request function.
	 * 
	 * @param client The client bank id.
	 * @param title of the transaction.
	 * @param value of the transaction. (positive)
	 * @return If the operation was successful and the server response.
	 * @throws IOException if an error occurs while asking the rest server.
	 */
	public Result payBack(int client, String title, double value) throws IOException {
		return postRequest(client, title, value);
	}
}
