package transactions;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Web error data.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Error implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7271751511995039928L;

	/**
	 * Time of the error.
	 */
	private long timestamp;

	/**
	 * Error ID.
	 */
	private int status;

	/**
	 * Error name.
	 */
	private String error;

	/**
	 * Error message.
	 */
	private String message;
	
	/**
	 * Error path.
	 */
	private String path;
}
