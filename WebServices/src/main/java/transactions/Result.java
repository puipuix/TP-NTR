package transactions;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Result body.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Result implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6170845821064465250L;
	
	/**
	 * If the request was successful.
	 */
	private boolean ok;
	
	/**
	 * Response message.
	 */
	private String message;
	
	/**
	 * The created operation if the request was successful. Null otherwise.
	 */
	private Operation operation;
	
	/**
	 *  The error message if the request was not successful. Null otherwise.
	 */
	private Error error;
}
