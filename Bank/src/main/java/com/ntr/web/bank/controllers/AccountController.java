package com.ntr.web.bank.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ntr.web.bank.annotations.DeleteJson;
import com.ntr.web.bank.annotations.GetJson;
import com.ntr.web.bank.annotations.PostJson;
import com.ntr.web.bank.entities.Account;
import com.ntr.web.bank.entities.CreateAccount;
import com.ntr.web.bank.exceptions.ResourceNotFoundException;
import com.ntr.web.bank.repositories.AccountRepository;
import com.ntr.web.bank.repositories.OperationRepository;

/**
 * Controller to manage accounts.
 * 
 * @author Alexis Vernes
 *
 */
@RestController
public class AccountController {

	/**
	 * Account repository.
	 */
	@Autowired
	AccountRepository accountRepository;

	/**
	 * Operation repository.
	 */
	@Autowired
	OperationRepository operationRepository;

	/**
	 * Find account by id.
	 * 
	 * @param id of the account.
	 * @return The account of that id.
	 * @throws ResourceNotFoundException if no account with id is found.
	 */
	@GetJson("accounts/{id}")
	public Account findById(@PathVariable("id") Integer id) {
		return accountRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No account with id " + id));
	}

	/**
	 * Find all accounts.
	 * 
	 * @return All accounts.
	 */
	@GetJson("accounts")
	public List<Account> findAll() {
		return accountRepository.findAll();
	}

	/**
	 * Find accounts by first name.
	 * 
	 * @param firstName of the clients.
	 * @return All account with that first name.
	 */
	@GetJson("accounts/firstname/{firstName}")
	public List<Account> findByFirstName(@PathVariable("firstName") String firstName) {
		return accountRepository.findByFirstName(firstName);
	}

	/**
	 * Find accounts by last name.
	 * 
	 * @param lastName of the clients.
	 * @return All account with that last name.
	 */
	@GetJson("accounts/lastname/{lastName}")
	public List<Account> findByLastName(@PathVariable("lastName") String lastName) {
		return accountRepository.findByLastName(lastName);
	}

	/**
	 * Create a new account.
	 * 
	 * @param acc body.
	 * @return the created account.
	 */
	@PostJson("accounts")
	public Account createAccount(@Validated @RequestBody CreateAccount acc) {
		Account a = accountRepository.save(Account.builder()
				.firstName(acc.getFirstName())
				.lastName(acc.getLastName())
				.balance(0)
				.build());
		if (a.getOperations() == null) {
			a.setOperations(new ArrayList<>());
		}
		return a;
	}

	/**
	 * Delete an account and all it's operations.
	 * 
	 * @param id of the account.
	 * @return 200 or 404.
	 */
	@DeleteJson("accounts/{id}")
	public ResponseEntity<?> deleteAccount(@PathVariable("id") Integer id) {
		return accountRepository.findById(id)
				.map(account -> {
					account.getOperations().forEach(op -> {
						operationRepository.delete(op);
					});
					accountRepository.delete(account);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new ResourceNotFoundException("Account not found with id " + id));

	}
}
