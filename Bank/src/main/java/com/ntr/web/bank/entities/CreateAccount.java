package com.ntr.web.bank.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Body used to create a new account.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccount {
	/**
	 * Client first name.
	 */
	private String firstName;
	
	/**
	 * Client last name.
	 */
	private String lastName;
}
