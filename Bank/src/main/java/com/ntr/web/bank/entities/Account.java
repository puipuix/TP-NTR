package com.ntr.web.bank.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Bank Account.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Account {
	/**
	 * The id of the account.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	/**
	 * Client first name.
	 */
	private String firstName;

	/**
	 * Client last name.
	 */
	private String lastName;

	/**
	 * Client balance.
	 */
	private double balance;

	/**
	 * All operation made by this account.
	 */
	@OneToMany(mappedBy = "account")
	@JsonIgnoreProperties({ "account" })
	private List<Operation> operations;

	/**
	 * Add the value to the balance.
	 * 
	 * @param value to add.
	 */
	public void addBalance(double value) {
		balance += value;
	}
}
