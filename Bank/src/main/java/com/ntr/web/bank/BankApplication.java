package com.ntr.web.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main.
 * 
 * @author Alexis Vernes
 *
 */
@SpringBootApplication
@RestController
public class BankApplication {

	/**
	 * Main function.
	 * 
	 * @param args the application arguments.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}

	/**
	 * Prevent error 404.
	 * 
	 * @return Nothing.
	 */
	@GetMapping("/")
	public String index() {
		return "Index";
	}
}
