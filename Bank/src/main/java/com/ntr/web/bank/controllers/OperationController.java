package com.ntr.web.bank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ntr.web.bank.annotations.GetJson;
import com.ntr.web.bank.annotations.PostXml;
import com.ntr.web.bank.entities.Account;
import com.ntr.web.bank.entities.CreateOperation;
import com.ntr.web.bank.entities.Operation;
import com.ntr.web.bank.exceptions.BadRequestException;
import com.ntr.web.bank.exceptions.ResourceNotFoundException;
import com.ntr.web.bank.repositories.AccountRepository;
import com.ntr.web.bank.repositories.OperationRepository;

/**
 * Controller to manage operations.
 * 
 * @author Alexis Vernes
 *
 */
@RestController
public class OperationController {

	/**
	 * Account repository.
	 */
	@Autowired
	AccountRepository accountRepository;

	/**
	 * Operation repository.
	 */
	@Autowired
	OperationRepository operationRepository;

	/**
	 * Find operation by id.
	 * 
	 * @param id of the operation.
	 * @return The operation of that id.
	 * @throws ResourceNotFoundException if no operation with id is found.
	 */
	@GetJson("operations/{id}")
	public Operation findById(@PathVariable("id") Integer id) {
		return operationRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No operation with id " + id));
	}

	/**
	 * Find all operations.
	 * 
	 * @return All operations.
	 */
	@GetJson("operations")
	public List<Operation> findAll() {
		return operationRepository.findAll();
	}

	/**
	 * Find operations by title.
	 * 
	 * @param title of the operation.
	 * @return All operation that contain the parameter in their title.
	 */
	@GetJson("operations/title/{title}")
	public List<Operation> findOperationsByTitle(@PathVariable("title") String title) {
		return operationRepository.findByTitleContainingIgnoreCase(title);
	}

	/**
	 * Find operations by account.
	 * 
	 * @param id of the account.
	 * @return All operation of that account id.
	 * @throws ResourceNotFoundException if no account with id is found.
	 */
	@GetJson("operations/account/{id}")
	public List<Operation> findAccountOperations(@PathVariable("id") Integer id) {
		return accountRepository.findById(id)
				.flatMap(a -> Optional.of(a.getOperations()))
				.orElseThrow(() -> new ResourceNotFoundException("No account with id " + id));
	}

	/**
	 * Find operations by account and title.
	 * 
	 * @param id of the account.
	 * @param title of the operation.
	 * @return All operation of that account id that contain the parameter in their
	 *         title.
	 * @throws ResourceNotFoundException if no account with id is found.
	 */
	@GetJson("operations/account/{id}/title/{title}")
	public List<Operation> findAccountOperationsByTitle(@PathVariable("id") Integer id,
			@PathVariable("title") String title) {
		List<Operation> tmp = findAccountOperations(id);
		tmp.removeIf(o -> !o.getTitle().toLowerCase().contains(title.toLowerCase()));
		return tmp;
	}

	/**
	 * Create a new operation.
	 * 
	 * @param ope body.
	 * @return the created operation.
	 * @throws BadRequestException if the balance is to low.
	 */
	@PostXml("operations")
	public Operation createOperation(@Validated @RequestBody CreateOperation ope) {
		Account acc = ope.getAccount(accountRepository);
		if (acc.getBalance() + ope.getAmount() > -100) {
			acc.addBalance(ope.getAmount());
			accountRepository.save(acc);
			return operationRepository.save(Operation.builder()
					.title(ope.getTitle())
					.amount(ope.getAmount())
					.dat(ope.getDate())
					.account(acc)
					.build());
		} else {
			throw new BadRequestException("Balance to low");
		}
	}
	
	/**
	 * Update the title of the operation.
	 * 
	 * @param id of the operation.
	 * @param title of the operation.
	 * @return 200 or 404.
	 */
	@PutMapping("operations/{id}/title/{title}")
	public ResponseEntity<?> updateAccountLastName(@PathVariable("id") Integer id,
			@PathVariable("title") String title) {
		return operationRepository.findById(id)
				.map(operation -> {
					operation.setTitle(title);
					operationRepository.save(operation);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new ResourceNotFoundException("Operation not found with id " + id));

	}
}
