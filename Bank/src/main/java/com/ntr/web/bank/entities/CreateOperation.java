package com.ntr.web.bank.entities;

import java.util.Date;

import com.ntr.web.bank.exceptions.ResourceNotFoundException;
import com.ntr.web.bank.repositories.AccountRepository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Body used to create operations.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateOperation {
	/**
	 * Title of the operation.
	 */
	private String title;
	/**
	 * Amount of money added (positive) or deduced (negative).
	 */
	private double amount;
	/**
	 * Id.
	 */
	private int account;

	/**
	 * We find the account entity using the account id.
	 * 
	 * @param accountRepository the account repository.
	 * @return The account entity.
	 */
	public Account getAccount(AccountRepository accountRepository) {
		return accountRepository.findById(account)
				.orElseThrow(() -> new ResourceNotFoundException("No account with id " + account));
	}

	/**
	 * @return Now.
	 */
	public Date getDate() {
		return new Date();
	}
}
