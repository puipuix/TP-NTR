package com.ntr.web.bank.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.ntr.web.bank.entities.Operation;

/**
 * SQL request for operations.
 * 
 * @author Alexis Vernes
 *
 */
public interface OperationRepository extends JpaRepository<Operation, Integer> {

	/**
	 * 
	 * @param title of the operation.
	 * @return All operation that contain the parameter in their title.
	 */
	public List<Operation> findByTitleContainingIgnoreCase(@Param("title") String title);
}
