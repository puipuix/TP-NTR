package com.ntr.web.bank.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Bank operations.
 * 
 * @author Alexis Vernes
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Operation {

	/**
	 * The ID of the operation.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	/**
	 * Title of the operation.
	 */
	private String title;
	/**
	 * Amount of money added (positive) or deduced (negative).
	 */
	private double amount;
	
	/**
	 * The account of that operation.
	 */
	@ManyToOne
	@JsonIgnore
	private Account account;
	
	/**
	 * The date of the operation.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date dat;
	
	/**
	 * @return The id of the account of that operation.
	 */
	@JsonGetter("account")
	private int getAccountId() {
		return account.getId();
	}
}
