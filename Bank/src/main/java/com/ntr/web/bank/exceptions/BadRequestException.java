package com.ntr.web.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * Error 400 : Bad Request
 * 
 * @author Alexis Vernes
 * 
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6415246148313090319L;

	/**
	 * 
	 */
	public BadRequestException() {
		super();
	}

	/**
     * Constructs a new bad request exception with the specified detail
     * message, cause, suppression enabled or disabled, and writable
     * stack trace enabled or disabled.
     *
     * @param  message the detail message.
     * @param cause the cause.  (A {@code null} value is permitted,
     * and indicates that the cause is nonexistent or unknown.)
     * @param enableSuppression whether or not suppression is enabled
     *                          or disabled
     * @param writableStackTrace whether or not the stack trace should
     *                           be writable
     */
	public BadRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
     * Constructs a new bad request exception with the specified detail
     * message, cause.
     *
     * @param  message the detail message.
     * @param cause the cause.  (A {@code null} value is permitted,
     * and indicates that the cause is nonexistent or unknown.)
     */
	public BadRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
     * Constructs a new bad request exception with the specified detail
     * message.
     *
     * @param  message the detail message.
     */
	public BadRequestException(String message) {
		super(message);
	}

	/**
     * Constructs a new bad request exception with the specified cause.
     *
     * @param cause the cause.  (A {@code null} value is permitted,
     * and indicates that the cause is nonexistent or unknown.)
     */
	public BadRequestException(Throwable cause) {
		super(cause);
	}
}
