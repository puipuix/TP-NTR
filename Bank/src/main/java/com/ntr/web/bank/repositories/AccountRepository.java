package com.ntr.web.bank.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.ntr.web.bank.entities.Account;

/**
 * SQL request for account.
 * 
 * @author Alexis Vernes
 *
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {
	
	/**
	 * 
	 * @param firstName of the clients.
	 * @return All account with that first name.
	 */
	public List<Account> findByFirstName(@Param("firstName") String firstName);
	
	/**
	 * 
	 * @param lastName of the clients.
	 * @return All account with that last name.
	 */
	public List<Account> findByLastName(@Param("lastName") String lastName);
}
