INSERT INTO ACCOUNT (ID,FIRST_NAME,LAST_NAME,BALANCE) VALUES
  (1, 'Anakin', 'Skywalker', 0),
  (2, 'Luke', 'Skywalker', 1000),
  (3, 'Rey', 'Skywalker', 10000),
  (4, 'Cheev', 'Palpatine', -10),
  (5, 'Padme', 'Amidala', 0);
  
INSERT INTO OPERATION (ID,TITLE,AMOUNT,ACCOUNT_ID,DAT) VALUES
  (1, 'op1_credit_a1', 100, 1, CURRENT_TIMESTAMP),
  (2, 'op2_debit_a4', -10, 4, CURRENT_TIMESTAMP),
  (3, 'op3_debit_a1', -70, 1, CURRENT_TIMESTAMP),
  (4, 'op4_credit_a2', 1000, 2, CURRENT_TIMESTAMP),
  (5, 'op5_debit_a1', -30, 1, CURRENT_TIMESTAMP),
  (6, 'op6_credit_a3', 10000, 3, CURRENT_TIMESTAMP);