# TP – Nouvelles technologies de la répartition 2020-21 M1 TNSI FI

***L’objectif du TP est de fournir une "application" bancaire mettant en œuvre l’architecture spécifiée ci-dessous, en utilisant l’ensemble des technologies vues en cours.***

La banque dispose de deux serveurs distincts. Un serveur *Wildfly* (10.x minimum) et un serveur *Tomcat* (8.5.x minimum).

Sur le serveur Wildfly, la banque héberge des services **REST JSON** permettant à un client utilisant une application (créée par exemple par un éditeur tiers) de consulter le solde et les opérations réalisées sur son compte. Pour simplifier, nous pouvons admettre qu’un client = un compte bancaire. Créez des jeux de tests "en dur" pour simuler la présence de clients dans une base de donnée.

Ce serveur expose également un ensemble de services **REST XML** permettant de réaliser des opérations sur les comptes bancaires des clients (crédit ou débit d’un certain montant pour le client A par exemple) Le second serveur, Tomcat héberge des services utilisés par exemple par des sites web de commerce pour payer les commandes des clients. Ces services sont accessibles en **SOAP** et demandent le débit ou le remboursement sur des comptes de clients. Quand une demande est réalisée, ce serveur utilise les services XML du serveur Wildfly pour réaliser les opérations bancaires. On choisit de ne pas se préoccuper de la sécurité/confidentialité des échanges entre les serveurs.

La solution proposée doit pouvoir se coupler avec un site de commerce qui doit être developper par vous. Vous pouvez utiliser le même serveur Wildfly pour héberger la banque et votre site de commerce, ou un autre serveur. 

Vous proposerez, en plus des applications hébergées sur les serveurs, trois programmes permettant de tester les accès SOAP, JSON et XML proposés par les différents serveurs. Vous devez également écrire des tests JUnit tester les différents accès.

## Rapport
Le rapport en format PDF se situe à la racine du git.

Le dossier *Other* contient les documents utilisés par le rapport.

## Serveurs
La documentation de chaque serveur se trouve dans leur sous-dossier doc.

### Bank
http://localhost:8090/

Le dossier *Bank* contient les services spring XML / JSON permettant de récupérer des informations ou de réaliser des opérations bancaires.

### WebServices
http://localhost:8080/

Le dossier *WebServices* contient les services SOAP qui vont utiliser les services de la banque.

### Commerce
Le dossier *Commerce* contient les services de commerce.

#### Back
http://localhost:8060/

Le dossier *Commerce/Back* contient le serveur Python permettant de récupérer une liste de produits et de les acheter.

#### Front
http://localhost:8070/

Le dossier *Commerce/Front* contient l'application JavaScript (Vue.js) pour communiquer avec le serveur python.

## Applications
La documentation des applications se trouve dans leur sous-dossier doc.

### PostMan
Le dossier *PostMan* contient une collection v2.1 PostMan permettant de lancer toutes les requêtes disponibles vers les serveurs via le logiciel PostMan. 

### Python
Le dossier *Python* contient un programme python pour récupérer données bancaires d'un compte.

### CSharp
Le dossier *CSharp* contient un programme C# pour récupérer le solde bancaire d'un compte.
