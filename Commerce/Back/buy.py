import json
import cgi
import requests
from xml.etree import ElementTree
from distutils.util import strtobool

print("Access-Control-Allow-Origin: *")
print("Content-type: application/json; charset=utf-8\n")

form = cgi.FieldStorage()
client = int(form.getvalue("client"))
product = int(form.getvalue("product"))
amount = int(form.getvalue("amount"))
xml = """
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <debit>
            <client>%d</client>
            <title>%s</title>
            <value>%f</value>
        </debit>
    </soap:Body>
</soap:Envelope>
"""
def ns(node):
    return "{http://transactions}%s" % node

def js(ok, code, error, message):
    if ok:
        ok = 'true'
    else:
        ok = 'false'
    return '{"ok": %s, "code": %d, "error": "%s", "message":"%s"}' % (ok, code, error, message)

if client != None and product != None and amount != None:
    client = int(client)
    product = int(product)
    amount = int(amount)
    if amount > 0:
        f = open("products.json", "r")
        products = json.loads(f.read())
        f.close()
        tmp = None
        found = False
        for pr in products:
           if pr["id"] == product:
               found = True
               tmp = pr
               
        if found:
            if tmp["amount"] - amount >= 0:
                price = tmp["price"] * amount
                r = requests.post('http://localhost:8080/WebServices/services/Transaction',
                              headers={"Content-Type":"text/xml", "SOAPAction": ""},
                              data=xml % (client,"Debit for %d %s" % (amount,tmp["name"]),price))
                
                if 200 <= r.status_code and r.status_code < 300:
                    soap = ElementTree.fromstring(r.content)
                    response = soap[0][0][0]
                    if bool(strtobool(response.find(ns("ok")).text)):
                        tmp["amount"] = tmp["amount"] - amount
                        f = open("products.json", "w")
                        f.write(json.dumps(products, indent=2))
                        f.close()
                        print(js(True, 200, "Ok", "Done"))
                    else:
                        print(js(False, 400, "Bad Request", response.find(ns("error")).find(ns("message")).text))
                else:
                    try:
                        r.raise_for_status()
                    except requests.exceptions.RequestException as e:
                        print(js(False, 500, "Internal Server Error", e))
            else:
                print(js(False, 400, "Bad Request", "Not enough aviable"))
        else:
            print(js(False, 404, "Not Found", "Product not found"))
    else:
        print(js(False, 400, "Bad Request", "Amount <= 0"))
else:
    print(js(False, 400, "Bad Request", "Missing parameter"))