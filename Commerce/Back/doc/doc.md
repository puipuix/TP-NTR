# Commerce Python Server

## Scripts

**server.py**: Execute the script to start the server.

**products.py**: Server script to get all products. (Do not execute)

**buy.py**: Server script to buy a product. (Do not execute)

## API
All request can be executed with GET operations.

**/products.py**: Return a JSON list of all products.

**/buy.py?client=P1&product=P2&amount=P3**: Buy P3 product of ID P2 for the bank account of ID P1.