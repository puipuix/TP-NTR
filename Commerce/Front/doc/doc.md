# Commerce JavaScript Client

## Pages
**/about**: About page. (Empty)

**/products**: Display the list of products. (Default redirect page)

**/buy**: Form to buy a product. (Can only be accessed from /products)

## Sources
**api/productApi.js**: Containt static function to communicate with the Python server.

**components/Product.vue**: Display a product.

**router/index.js**: Define routes and redirections.

**views/About.vue**: About page.

**views/Home.vue**: Home page. Used as a container for subpages.

**views/List.vue**: List of products.

**views/Buy.vue**: Form to buy a product. If the product is undefined it will redirect to /products.