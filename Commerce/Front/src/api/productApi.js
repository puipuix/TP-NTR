import Vue from 'vue'

export default class ProductApi {
  static getProducts() {
    return Vue.http.get("http://localhost:8060/products.py")
  }
  static buyProduct(client, product, amount) {
    return Vue.http.get("http://localhost:8060/buy.py?client="+client+"&product=" + product.id + "&amount=" + amount)
  }
}
