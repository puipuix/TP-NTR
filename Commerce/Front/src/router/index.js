import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import List from '../views/List.vue'
import Buy from '../views/Buy.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/products',
        name: 'Products',
        component: List
      },
      {
        path: '/buy',
        name: 'Buy',
        component: Buy,
        props: route => ({ product: route.query.product })
      },
      {
        path: '/',
        redirect: '/products'
      }
    ]
  }, {
    path: '*',
    redirect: '/products'
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
